%Joel Castro
clear
clc


%SIRFIT1:
S=[762 740 650 400 250 120 80 50 20 18 15 13 10];
I=[1 20 80 220 300 260 240 190 120 80 20 5 2];
days=[0 3 4 5 6 7 8 9 10 11 12 13 14];
for k=1:10
Y(k)=(1/I(k+2))*(I(k+3)-I(k+1))/2;
X(k)=S(k+2);
end
c = polyfit(X,Y,1);
a = c(1);
b = -c(2);
fprintf('a is %f and b is %f \n',a,b);
    % a is 0.003636 and b is 0.939470 